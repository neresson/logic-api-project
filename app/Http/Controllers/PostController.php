<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use App\Models\Post;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
    * @OA\Get(
    * path="/posts",
    * summary="Get last posts",
    * description="Get last posts available on the resource",
    * operationId="index",
    * tags={"Posts"},
    * @OA\Response(
    *    response=200,
    *    description="successfull operation",
    *    @OA\JsonContent(
    *       @OA\Property(property="title", type="string", example="My post 1"),
    *       @OA\Property(property="slug", type="string", example="my-post-1"),
    *       @OA\Property(property="content", type="string", example="This is a content of my post!"),
    *       @OA\Property(property="author", type="object", example="1"),
    *       @OA\Property(property="created_at", type="string", example="2021-04-09T10:31:14.000000Z"),
    *       @OA\Property(property="updated_at", type="string", example="2021-04-09T10:46:26.000000Z"),
    *       @OA\Property(property="id", type="integer", example="5")
    *        )
    *     )
    * )
    */
    public function index()
    {
        return Post::latest()->get();
    }

    /**
    * @OA\Post(
    * path="/posts",
    * summary="Create a post",
    * description="Get all posts available on the resource",
    * operationId="store",
    * tags={"Posts"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Pass post content",
    *    @OA\JsonContent(
    *       required={"title","slug"},
    *       @OA\Property(property="slug", type="string", example="my-post-1"),
    *       @OA\Property(property="title", type="string", example="My post 1"),
    *       @OA\Property(property="content", type="string", example="This is a content of my post!"),
    *    ),
    * ),
    * @OA\Response(
    *    response=201,
    *    description="successfull operation",
    *    @OA\JsonContent(
    *       @OA\Property(property="title", type="string", example="My post 1"),
    *       @OA\Property(property="slug", type="string", example="my-post-1"),
    *       @OA\Property(property="content", type="string", example="This is a content of my post!"),
    *        @OA\Property(property="author", type="object", example="1"),
    *       @OA\Property(property="created_at", type="string", example="2021-04-09T10:31:14.000000Z"),
    *       @OA\Property(property="updated_at", type="string", example="2021-04-09T10:46:26.000000Z"),
    *       @OA\Property(property="id", type="integer", example="5")
    *        )
    *     ),
    * @OA\Response(
    *    response=401,
    *    description="authorization error"),
    * @OA\Response(
    *    response=422,
    *    description="missing header or slug"),
    * security={{ "apiAuth": {} }}
    * )
    */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $user = request()->user();
        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'author' => $user->id
        ]);

        return $post;
    }

    /**
    * @OA\Get(
    * path="/posts/{post}",
    * summary="Get single post",
    * description="Get single post by id",
    * operationId="show",
    * tags={"Posts"},
    * @OA\Parameter(
    *    description="ID of post",
    *    in="path",
    *    name="post",
    *    required=true,
    *    example="1"
    * ),
    * @OA\Response(
    *    response=200,
    *    description="successfull operation",
    *    @OA\JsonContent(
    *       @OA\Property(property="title", type="string", example="My post 1"),
    *       @OA\Property(property="slug", type="string", example="my-post-1"),
    *       @OA\Property(property="content", type="string", example="This is a content of my post!"),
    *        @OA\Property(property="author", type="object", example="1"),
    *       @OA\Property(property="created_at", type="string", example="2021-04-09T10:31:14.000000Z"),
    *       @OA\Property(property="updated_at", type="string", example="2021-04-09T10:46:26.000000Z"),
    *       @OA\Property(property="id", type="integer", example="5")
    *        )
    *     )
    * )
    */
    public function show(Post $post)
    {
        return $post;
    }

    /**
    * @OA\Get(
    * path="/posts/find_by_author/{author_id}",
    * summary="Get all posts from author",
    * description="Get single post by id",
    * operationId="show_by_author",
    * tags={"Posts"},
    * @OA\Parameter(
    *    description="ID of author",
    *    in="path",
    *    name="author_id",
    *    required=true,
    *    example="1"
    * ),
    * @OA\Response(
    *    response=200,
    *    description="successfull operation",
    *    @OA\JsonContent(
    *       @OA\Property(property="title", type="string", example="My post 1"),
    *       @OA\Property(property="slug", type="string", example="my-post-1"),
    *       @OA\Property(property="content", type="string", example="This is a content of my post!"),
    *        @OA\Property(property="author", type="object", example="1"),
    *       @OA\Property(property="created_at", type="string", example="2021-04-09T10:31:14.000000Z"),
    *       @OA\Property(property="updated_at", type="string", example="2021-04-09T10:46:26.000000Z"),
    *       @OA\Property(property="id", type="integer", example="5")
    *        )
    *     )
    * )
    */

    public function showByAuthorId($userId)
    {
        return Post::where('author', $userId)->firstOrFail();
    }
    /**
    * @OA\Put(
    * path="/posts/{post}",
    * summary="Update a post",
    * description="Get all posts available on the resource",
    * operationId="update",
    * tags={"Posts"},
    * @OA\Parameter(
    *    description="ID of post",
    *    in="path",
    *    name="post",
    *    required=true,
    *    example="1"
    * ),
    * @OA\RequestBody(
    *    required=true,
    *    description="Pass post content",
    *    @OA\JsonContent(
    *       required={"title","slug"},
    *       @OA\Property(property="slug", type="string", example="my-post-1"),
    *       @OA\Property(property="title", type="string", example="My post 1"),
    *       @OA\Property(property="content", type="string", example="This is a content of my post!"),
    *    ),
    * ),
    * @OA\Response(
    *    response=200,
    *    description="successfull operation",
    *    @OA\JsonContent(
    *       @OA\Property(property="title", type="string", example="My post 1"),
    *       @OA\Property(property="slug", type="string", example="my-post-1"),
    *       @OA\Property(property="content", type="string", example="This is a content of my post!"),
    *       @OA\Property(property="author", type="object", example="1"),
    *       @OA\Property(property="created_at", type="string", example="2021-04-09T10:31:14.000000Z"),
    *       @OA\Property(property="updated_at", type="string", example="2021-04-09T10:46:26.000000Z"),
    *       @OA\Property(property="id", type="integer", example="5")
    *        )
    *     ),
    * @OA\Response(
    *    response=401,
    *    description="authorization error"),
    * @OA\Response(
    *    response=422,
    *    description="missing header or slug"),
    * security={{ "apiAuth": {} }}
    * )
    */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $post -> update([
            'title' => $request->title,
            'content' => $request->content,
        ]);
        $post->touch();

        return $post;
    }

    /**
    * @OA\Delete(
    * path="/posts/{post}",
    * summary="Delete a post",
    * description="Delete single post",
    * operationId="destroy",
    * tags={"Posts"},
    * security={{ "apiAuth": {} }},
    * @OA\Parameter(
    *    description="ID of post",
    *    in="path",
    *    name="post",
    *    required=true,
    *    example="1"
    * ),
    * @OA\Response(
    *    response=200,
    *    description="successfull operation",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Deleted successfully"),
    *        )
    *     )
    * ),
    */
    public function destroy($id)
    {
        Post::destroy($id);

        return response()->json([
            'message' => 'Deleted successfully'
        ]);
    }
}
