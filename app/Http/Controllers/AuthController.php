<?php namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum', ['except' => ['login', 'registration']]);
    }

    /**
    * @OA\Post(
    * path="/user/login",
    * summary="Login",
    * description="Login and get a refreshed bearer token",
    * operationId="login",
    * tags={"Users"},
    * @OA\Parameter(
    *    description="email",
    *    in="query",
    *    name="email",
    *    required=true,
    *    example="test@example.ru2"
    * ),
    * @OA\Parameter(
    *    description="password",
    *    in="query",
    *    name="password",
    *    required=true,
    *    example="superpassword"
    * ),
    * @OA\Response(
    *    response=200,
    *    description="successfull login",
    *    @OA\JsonContent(
    *       @OA\Property(property="token", type="string", example="9|tfeZSZvib4cd953Oq9J8j7qMNPe8Eb5HtYcDHwFn"),
    *        )
    *     )
    * ),
    * @OA\Response(
    *    response=401,
    *    description="authorization error"),
    */
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }
        $token = $user->createToken('myapptoken')->plainTextToken;
        return response()->json(['token' => $token]);;
    }

    /**
    * @OA\Post(
    * path="/user/registration",
    * summary="registration",
    * description="Register and get an account",
    * operationId="Registration",
    * tags={"Users"},
    * @OA\Parameter(
    *    description="name",
    *    in="query",
    *    name="name",
    *    required=true,
    *    example="testuser-1"
    * ),
    * @OA\Parameter(
    *    description="email",
    *    in="query",
    *    name="email",
    *    required=true,
    *    example="test@example.ru2"
    * ),
    * @OA\Parameter(
    *    description="password",
    *    in="query",
    *    name="password",
    *    required=true,
    *    example="superpassword"
    * ),
    * @OA\Response(
    *    response=200,
    *    description="successfull registration",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Successfull registration!"),
    *       @OA\Property(property="token", type="string", example="9|tfeZSZvib4cd953Oq9J8j7qMNPe8Eb5HtYcDHwFn")
    *     )
    *   )
    * )
    */
    public function registration()
    {
        $name = request('name');
        $email = request('email');
        $password = request('password');

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->save();

        $token = $user->createToken('myapptoken')->plainTextToken;

        return response()->json(['message' => 'Successfull registration!',
                                 'token' => $token]);
    }

    /**
    * @OA\Get(
    * path="/user/me",
    * summary="Me",
    * description="Get info about your account",
    * operationId="Me",
    * tags={"Users"},
    * security={{ "apiAuth": {} }},
    * @OA\Response(
    *    response=200,
    *    description="got info",
    *    @OA\JsonContent(
    *       @OA\Property(property="id", type="indeger", example="3"),
    *       @OA\Property(property="name", type="string", example="testuser1"),
    *       @OA\Property(property="email", type="string", example="test@example.com"),
    *       @OA\Property(property="created_at", type="string", example="2021-04-09T10:42:27.000000Z"),
    *       @OA\Property(property="updated_at", type="string", example="2021-04-09T10:42:27.000000Z"),
    *        )
    *     )
    * )
    */
    public function me()
    {
        return request()->user();
    }

    /**
    * @OA\Get(
    * path="/user/user_info/{user_id}",
    * summary="User info",
    * description="Get info about account",
    * operationId="UserInfo",
    * tags={"Users"},
    * security={{ "apiAuth": {} }},
    * @OA\Parameter(
    *    description="ID of user",
    *    in="path",
    *    name="user_id",
    *    required=true,
    *    example="1"
    * ),
    * @OA\Response(
    *    response=200,
    *    description="got info",
    *    @OA\JsonContent(
    *       @OA\Property(property="id", type="indeger", example="3"),
    *       @OA\Property(property="name", type="string", example="testuser1"),
    *       @OA\Property(property="email", type="string", example="test@example.com"),
    *       @OA\Property(property="created_at", type="string", example="2021-04-09T10:42:27.000000Z"),
    *       @OA\Property(property="updated_at", type="string", example="2021-04-09T10:42:27.000000Z"),
    *        )
    *     )
    * )
    */
    public function userInfo($id)
    {
        return User::find($id);
    }

    /**
    * @OA\Post(
    * path="/user/logout",
    * summary="Logout",
    * description="Logout and deathorize bearer token",
    * operationId="logout",
    * tags={"Users"},
    * security={{ "apiAuth": {} }},
    * @OA\Response(
    *    response=200,
    *    description="successfull logout",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Successfully logged out"),
    *        )
    *     )
    * )
    */
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json(['message' => 'Successfully logged out']);
    }
}
