<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="OpenApi posts documentation",
     *      description="this is a super simple posts api",
     *      @OA\Contact(
     *          email="donskirill@me.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server"
     * )
     *
     * @OA\SecurityScheme(
     *      securityScheme="apiAuth",
     *      type="http",
     *      description="Login with email and password to get the authentication token",
     *      name="Token based authentification",
     *      in="header",
     *      bearerFormat="JWT",
     *      scheme="bearer"
     * )
     */
}
