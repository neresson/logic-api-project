<?php

namespace Tests\Featue;

use Tests\TestCase;
use App\Models\User;
use App\Models\Post;
use Laravel\Sanctum\Sanctum;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostsTest extends TestCase
{
    use RefreshDatabase;

    public function testCreatingPost()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->postJson('/api/posts', [
                            'title' => 'creating post test title',
                            'content' => 'creating post test content'
                         ]);
        $response
            ->assertStatus(201)
            ->assertJson(
                fn (AssertableJson $json) =>
                 $json->where('id', 1)
                      ->where('title', 'creating post test title')
                      ->where('content', 'creating post test content')
                      ->etc()
            );
    }

    public function testCannotCreatePostUnauthenticated()
    {
        $response = $this->postJson('/api/posts', [
                            'title' => 'creating post test title unauth',
                            'content' => 'creating post test content unauth'
                         ]);
        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testMakingIndexRequest()
    {
        for ($i = 1; $i <= 12; $i++) {
            Post::create([
                'title' => "title #$i",
                'content' => "content #$i",
                'author' => 1
            ]);
        }

        $response = $this->getJson('/api/posts');
        $response
            ->assertStatus(200)
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has(12)
                     ->first(
                         fn ($json) =>
                        $json->where('id', 2)
                             ->where('title', 'title #1')
                             ->where('content', 'content #1')
                             ->etc()
                     )
            );
    }

    public function testGetSinglePost()
    {
        $post = Post::create([
            'title' => 'single post title',
            'content' => 'single post content',
            'author' => 1
        ]);

        $response = $this->getJson("/api/posts/$post->id");
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                 $json->where('id', $post->id)
                      ->where('title', $post->title)
                      ->where('content', $post->content)
                      ->etc()
            );
    }

    public function testCannotGetSinglePost()
    {

        $response = $this->getJson("/api/posts/1");
        $response
            ->assertstatus(404);
    }

    public function testUpdatePost()
    {
        Sanctum::actingAs(
            $user = User::factory()->create()
        );

        $post = Post::create([
            'title' => 'before update post title',
            'content' => 'before update post content',
            'author' => $user->id
        ]);

        $response = $this->putJson("/api/posts/$post->id", [
                            'title' => 'after update post title',
                            'content' => 'after update post content'
                         ]);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                 $json->where('id', $post->id)
                      ->where('title', 'after update post title')
                      ->where('content', 'after update post content')
                      ->etc()
            );
    }

    public function testCannotUpdatePostUnauthenticated()
    {
        $response = $this->putJson('/api/posts/2', [
                            'title' => 'updating post test title',
                            'content' => 'updating post test content'
                         ]);

        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testDeletePost()
    {
        Sanctum::actingAs(
            $user = User::factory()->create()
        );

        $post = Post::create([
            'title' => 'before update post title',
            'content' => 'before update post content',
            'author' => $user->id
        ]);

        $response = $this->deleteJson("/api/posts/$post->id");
        $response->assertJson([
            'message' => 'Deleted successfully'
        ]);
        $this->assertDatabaseMissing('posts', [
            'id' => $post->id,
        ]);
    }

    public function testCannotDeletePostUnauthenticated()
    {
        $response = $this->deleteJson('/api/posts/2');
        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testGetPostsFromAuthor()
    {
        Sanctum::actingAs(
            $user = User::factory()->create()
        );
        $post = Post::create([
            'title' => 'title from author',
            'content' => 'content from author',
            'author' => $user->id
        ]);

        $response = $this->getJson("/api/posts/find_by_author/$post->author");
        $response
            ->assertStatus(200)
            ->assertJson(
                fn (AssertableJson $json) =>
                    $json->where('id', $post->id)
                         ->where('title', $post->title)
                         ->where('content', $post->content)
                         ->where('author', $user->id)
                         ->etc()

            );
    }

    public function testCannotGetPostsFromAuthorWithNoPosts()
    {
        $response = $this->getJson('/api/posts/find_by_author/2');

        $response->assertStatus(404);
    }
}
