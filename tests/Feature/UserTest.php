<?php

namespace Tests\Featue;

use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateAUser()
    {
        $response = $this->postJson('/api/user/registration', [
            'name' => 'test_name',
            'email' => 'test@email.com',
            'password' => 'password'
        ]);
        $response->assertJson([
                    'message' => 'Successfull registration!'
                   ]);

        $this->assertDatabaseHas('users', [
            'email' => 'test@email.com',
        ]);
    }

    public function testLogin()
    {
        $password = 'password';
        $user = new User();
        $user->name = 'test_name';
        $user->email = 'test@email.com';
        $user->password = Hash::make($password);
        $user->save();

        $response = $this->postJson('/api/user/login', [
            'email' => $user->email,
            'password' => $password
        ]);
        $response->assertJsonStructure([
            'token'
        ]);
    }

    public function testInfoAboutMe()
    {
        Sanctum::actingAs(
            $user = User::factory()->create()
        );
        $response = $this->getJson('/api/user/me');
        $response->assertJson([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email
           ]);
    }

    public function testCannotGetInfoAboutMeUnathenticated()
    {
        $response = $this->getJson('/api/user/me');
        $response->assertJson([
            'message' => 'Unauthenticated.'
           ]);
    }

    public function testInfoAboutAnotherUser()
    {
        $anotherUser = User::factory()->create();

        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->getJson("/api/user/user_info/$anotherUser->id");
        $response->assertJson([
            'id' => $anotherUser->id,
            'name' => $anotherUser->name,
            'email' => $anotherUser->email
           ]);
    }

    public function testCannotGetInfoAboutAnotherUserUnathenticated()
    {
        $response = $this->getJson('/api/user/user_info/1');
        $response->assertJson([
            'message' => 'Unauthenticated.'
           ]);
    }

    public function testLogout()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->postJson('/api/user/logout');
        $response->assertJson([
            'message' => 'Successfully logged out'
        ]);
    }

    public function testCannotLogoutUnathenticated()
    {
        $response = $this->postJson('/api/user/logout');
        $response->assertJson([
            'message' => 'Unauthenticated.'
           ]);
    }
}
